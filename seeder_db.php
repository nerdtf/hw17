<?php
require_once 'db_connection.php';

$articles = [
	[
		'title' => 'Text 1',
		'ttext' => 'some text some text some text some text some text some text some text some text some text '

	], 

	[
		'title' => 'Text 2',
		'ttext' => 'some text some text some text some text some text some text some text some text some text '

	],

	[
		'title' => 'Text 3',
		'ttext' => 'some text some text some text some text some text some text some text some text some text '

	],

	[
		'title' => 'Text 4',
		'ttext' => 'some text some text some text some text some text some text some text some text some text '

	],

	[
		'title' => 'Text 5',
		'ttext' => 'some text some text some text some text some text some text some text some text some text '

	]
];
try{
	$sql = 'INSERT INTO articles SET 
		title = :title,
		ttext = :ttext		
	';
$pdoStatement = $pdo->prepare($sql);
foreach ($articles as $article) {
	$pdoStatement->bindValue(':title' , $article['title']);
	$pdoStatement->bindValue(':ttext' , $article['ttext']);
	$pdoStatement->execute();
}
echo 'Test data added';
die();

}catch(Exception $e){
	echo 'Error adding test data' . $e->getMessage();
}