<meta charset="utf-8">
<?php
require_once 'db_connection.php';
try{
	$sql = 'CREATE TABLE articles(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		title VARCHAR(255) NOT NULL,
		ttext VARCHAR(255)		
	)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
	$pdo->exec($sql);

}catch(Exception $e){
	echo "Не удалось создать таблицу" . $e->getMessage();
}